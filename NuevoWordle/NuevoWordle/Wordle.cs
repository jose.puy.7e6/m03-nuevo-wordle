﻿using System;
using System.IO;

namespace NuevoWordle
{
    /// <summary>
    /// Creacion de la clase para guardar el historial.
    /// </summary>
    public class Record
    {
        public string name;
        public int tries;
        public string word;
    }

    class Wordle
    {
        /// <summary>
        /// Inicio del programa, definiendo la ruta inicial, el idioma predeterminado y el numero de intentos maximos.
        /// </summary>
        static void Main()
        {
            string path = @"../../../../../Files/";
            Directory.SetCurrentDirectory(path);
            string lang = @"lang/es";
            int maxTries = 6;

            Menu(lang, maxTries);
        }

        /// <summary>
        /// Menu el cual nos permite usar todas las opciones.
        /// </summary>
        static void Menu(string lang, int maxTries)
        {
            int input;
            do
            {
                do
                {
                    Console.Clear();
                    ShowTitle();
                    ShowOptions(lang);
                    Console.Write("\n>");
                } while (!int.TryParse(Console.ReadLine(), out input));
                switch (input)
                {
                    case 1:
                        Play(lang, maxTries);
                        break;
                    case 2:
                        ShowInstructions(lang);
                        break;
                    case 3:
                        ShowHistory(lang);
                        break;
                    case 4:
                        lang = LangMenu(lang);
                        break;
                }
            } while (!(input == 5));
        }

        /// <summary>
        /// Mostrar el titulo ASCII.
        /// </summary>
        static void ShowTitle()
        {
            StreamReader sr = new StreamReader("title.txt");
            string[] title = sr.ReadToEnd().Split(";");
            sr.Close();
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(title[0]);
            Console.ResetColor();
            Console.WriteLine(title[1]);
        }

        /// <summary>
        /// Mostrar las opciones del menu disponibles.
        /// </summary>
        static void ShowOptions(string lang)
        {
            string path = lang + @"\menu.txt";
            string text = Reader(path);

            Console.WriteLine(text);
        }

        /// <summary>
        /// Mostrar las instrucciones del juego.
        /// </summary>
        static void ShowInstructions(string lang)
        {
            Console.Clear();
            string file = lang + "_instruction.txt";

            StreamReader sr = new StreamReader(file);
            string[] instructions = sr.ReadToEnd().Split(";");
            sr.Close();
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine(instructions[0]);
            Console.ResetColor();
            Console.WriteLine(instructions[1]);
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine(instructions[2]);
            Console.ResetColor();

            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write("\n X ");
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.Write(" I ");
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.Write(" C O T ");
            Console.ResetColor();

            Console.WriteLine(instructions[3]);

            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.Green;
            Console.Write(" C ");
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.Write(" A I ");
            Console.BackgroundColor = ConsoleColor.Green;
            Console.Write(" X ");
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.Write(" A ");
            Console.ResetColor();

            Console.WriteLine(instructions[4]);

            Console.WriteLine();
            Console.Write("\n>");
            Console.ReadLine();
        }

        /// <summary>
        /// Abrir el menu de idiomas.
        /// </summary>
        static string LangMenu(string lang)
        {
            do
            {
                int output;
                do
                {
                    ShowLang();
                } while (!int.TryParse(Console.ReadLine(), out output));
                switch (output)
                {
                    case 1:
                        return "lang/es";
                    case 2:
                        return "lang/cat";
                    case 3:
                        return "lang/en";
                    default:
                        switch (lang)
                        {
                            case "lang/es":
                                Console.WriteLine("No es una opción válida");
                                break;
                            case "lang/cat":
                                Console.WriteLine("No és una opció válida");
                                break;
                            case "lang/en":
                                Console.WriteLine("It's not a valid option");
                                break;
                        }
                        break;
                }
            } while (true);
        }

        /// <summary>
        /// Mostrar los idiomas disponibles.
        /// </summary>
        static void ShowLang()
        {
            Console.WriteLine("\t1. Español\n" +
                "\t2. Català\n" +
                "\t3. English\n");
            Console.Write(">");
        }

        /// <summary>
        /// Metodo completo del juego.
        /// </summary>
        static void Play(string lang, int maxTries)
        {
            Console.Clear();
            int tries = 0;
            string word = GetWord(lang);
            Console.WriteLine(word);

            ShowTriesText(lang);

            do
            {
                string input;
                do
                {
                    Console.Write(">");
                    input = Console.ReadLine().ToUpper();
                    if (!(input.Length == 5))
                    {
                        LengthError(lang);
                    }
                } while (input.Length != 5);

                Console.WriteLine("");
                TryWord(word, input);
                Console.WriteLine("\n");

                if (input == word) break;

                if (input != word && tries != 6) tries = CurrentTries(tries, maxTries, lang);
            } while (tries != 6);

            if (tries == 6)
            {
                Defeat(lang, word);
            }
            else
            {
                Victory(lang);
            }

            SaveHistory(lang, tries, word);

            Console.ReadLine();
        }

        /// <summary>
        /// Conseguir una palabra aleatoria de un fichero determinado.
        /// </summary>
        static string GetWord(string lang)
        {
            string wordFile = lang + @"\words.txt";
            Random rnd = new Random();
            StreamReader sr = new StreamReader(wordFile);
            string[] allWords = sr.ReadToEnd().Split(",");
            int num = rnd.Next(0, allWords.Length);
            return allWords[num].ToUpper();
        }

        /// <summary>
        /// Mensaje de error de longitud e la palabra.
        /// </summary>
        static void LengthError(string lang)
        {
            string path = lang + @"\error.txt";
            string text = Reader(path);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(text);
            Console.ResetColor();
        }

        /// <summary>
        /// Mostrar mensaje de victoria.
        /// </summary>
        static void Victory(string lang)
        {
            string path = lang + @"\victory.txt";
            string text = Reader(path);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(text);
            Console.ResetColor();
        }

        /// <summary>
        /// Mostrar mensaje de derrota.
        /// </summary>
        static void Defeat(string lang, string word)
        {
            string path = lang + @"\defeat.txt";
            string text = Reader(path);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(text);
            string fullText = Reader(@"separator.txt");
            string[] textS = fullText.Split(";");
            Console.Write(textS[0] + "   " + word.ToUpper() + "   " + textS[0] + "\n");
            Console.ResetColor();
        }

        /// <summary>
        /// Metodo para leer el contenido de un fichero dependiendo del lenguaje elegido.
        /// </summary>
        static string Reader(string path)
        {
            StreamReader sr = new StreamReader(path);
            string text = sr.ReadToEnd();
            sr.Close();
            return text;
        }

        /// <summary>
        /// Metodo para comprobar si la palabra secreta contiene letras de la introducida.
        /// </summary>
        static void TryWord(string word, string input)
        {
            input = input.ToUpper();
            Console.ForegroundColor = ConsoleColor.Black;
            for (int i = 0; i < word.Length; i++)
            {
                bool lletraEsta = false;

                if (input[i] == word[i])
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.Write($" {input[i]} ");
                }
                else
                {
                    for (int j = 0; j < word.Length; j++)
                    {
                        if (input[i] == word[j]) lletraEsta = true;
                    }
                    if (lletraEsta == true)
                    {
                        Console.BackgroundColor = ConsoleColor.Yellow;
                        Console.Write($" {input[i]} ");
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.Write($" {input[i]} ");
                    }
                }
            }
            Console.ResetColor();
        }

        /// <summary>
        /// Mostrar cuantos intentos le quedan al usuario.
        /// </summary>
        static void ShowTriesText(string lang)
        {
            string path = lang + @"\tries.txt";
            string text = Reader(path);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(text);
            Console.ResetColor();
        }

        /// <summary>
        /// Metodo para calcular en numero de intentos.
        /// </summary>
        static int CurrentTries(int tries, int maxTries, string lang)
        {
            tries++;
            int diff = maxTries - tries;
            if (diff > 0)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                string path = lang + @"\currentTries.txt";
                string fullText = Reader(path);
                string[] text = fullText.Split(";");
                Console.Write(text[0] + diff + text[1] + "\n");
                Console.ResetColor();
            }
            return tries;
        }

        /// <summary>
        /// Mostrar el Historial de partidas guardado.
        /// </summary>
        static void ShowHistory(string lang)
        {
            Console.Clear();
            string historyPath = "history.txt";
            if (!File.Exists(historyPath))
            {
                HistoryError(lang);
            }
            else
            {
                string path = lang + @"\historyText.txt";
                string title = Reader(path);
                Console.WriteLine(title);
                StreamReader sr = new StreamReader(historyPath);
                string[] totalHistory = sr.ReadToEnd().Replace("\n", "").Split("\r");
                sr.Close();
                foreach (string line in totalHistory)
                {
                    string[] record = line.Split(",");
                    if (!(line == ""))
                    {
                        if (record[2] == "7")
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("\t" + record[0] + ": " + record[1]);
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("\t" + record[0] + ": " + record[1] + " " + record[2]);
                        }
                    }
                    Console.ResetColor();
                }
            }
            Console.Write("\n>");
            Console.ReadLine();
        }

        /// <summary>
        /// Metodo para guardar el historial al terminar una partida.
        /// </summary>
        static void SaveHistory(string lang, int tries, string word)
        {
            string historyPath = "history.txt";
            string path = lang + @"\record.txt";
            string text = Reader(path);
            Console.Write(text);
            Record writer = new Record()
            {
                name = Console.ReadLine(),

                tries = tries + 1,

                word = word
            };
            if (!File.Exists(historyPath))
            {
                File.Create(historyPath).Close();
            }
            StreamWriter sw = File.AppendText(historyPath);
            sw.WriteLine(writer.name + "," + writer.word + "," + writer.tries);
            sw.Close();
        }

        /// <summary>
        /// Mostrar error en caso de no existir el fichero historial.
        /// </summary>
        static void HistoryError(string lang)
        {
            string path = lang + @"\historyError.txt";
            string text = Reader(path);
            Console.Write(text);
        }
    }
}
